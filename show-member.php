<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Show member</title>
    <link rel="shortcut icon" type="image/png" href="public/images/favicon.png"/>
    <link rel="stylesheet" href="public/libs/font-awesome-5/css/all.css">
    <link rel="stylesheet" href="public/libs/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="public/build/css/style.css">
</head>
<body>
    <!-- header -->
    <div class="html-template" w3-include-html="layout/header.html"></div>
    <!-- /header -->
    <!-- body -->
    <div class="cw-body">
        <!-- menu -->
        <div class="html-template" w3-include-html="layout/menu.html"></div>
        <!-- /menu -->
        <!-- content -->
        <div class="cw-body-right cw-content">
            <div class="cw-workspace-header">
                <div class="cw-workspace-intro">
                    <div class="cw-workspace-avatar">
                        <img src="public/images/workspace/avatar1.jpg" alt="workspace's avatar">
                    </div>
                    <div class="cw-workspace-name cw-normal-header">
                        CapstoneProject-ClockWorks
                    </div>
                </div>
                <ul class="cw-tabs">
                    <li rel="process">
                        <i class="fas fa-sitemap"></i>&nbsp;Processes&nbsp;<span class="cw-tab-quantity">0</span>
                    </li>
                    <li class="cw-active-tab" rel="members">
                        <i class="fas fa-user-friends"></i>&nbsp;Members&nbsp;<span class="cw-tab-quantity">1</span>
                    </li>
                    <li rel="settings">
                        <i class="fas fa-cog"></i>&nbsp;Settings
                    </li>
                </ul>
            </div>
            <div class="cw-container" style="position:relative">
                <div class="cw-find-members">
                    <div class="form-group">
                        <span class="cw-input-icon"> 
                            <input type="text" name="name" class="form-control cw-input-has-icon" id="cw-members-name" placeholder="Find a member...">
                            <i class="fas fa-search cw-label-icon"></i>
                        </span>
                        <button class="btn btn-primary">Members</button>
                        <button class="btn btn-success" style="float: right">Invite members</button>
                        <div class="clearfix"></div>    
                    </div>
                </div>
                <div class="cw-workspace-members-list-detail">
                    <table class="cw-table-box cw-table-gray table table-hover">
                        <thead>
                            <tr class="cw-row">
                                <th class="cw-column" colspan="4">
                                    <span style="float: left">
                                        <label>
                                            <input type="checkbox" name="" id=""> Select all
                                        </label>
                                    </span>
                                    <span style="float: right">
                                        Role
                                    </span>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="cw-row">
                                <td class="cw-column">
                                    <input type="checkbox" name="" id="">
                                    <div class="cw-member-avatar">
                                        <img src="public/images/user/avatar1.jpg" alt="member's avatar" width="40" height="40">
                                    </div>
                                    <a href="#">PhuongDuy</a>
                                </td>
                                <td class="cw-column">
                                    Duyho@gmail.com
                                </td>
                                <td class="cw-column">
                                    Owner
                                </td>
                                <td class="cw-column">
                                    <button class="btn btn-default"><i class="fas fa-cog"></i></button>
                                </td>
                            </tr>
                            <tr class="cw-row">
                                <td class="cw-column">
                                    <input type="checkbox" name="" id="">
                                    <div class="cw-member-avatar">
                                        <img src="public/images/user/avatar1.jpg" alt="member's avatar" width="40" height="40">
                                    </div>
                                    <a href="#">AnNguyen</a>
                                </td>
                                <td class="cw-column">
                                    annguyen115@vanlanguni.vn
                                </td>
                                <td class="cw-column">
                                    Manager
                                </td>
                                <td class="cw-column">
                                    <button class="btn btn-default"><i class="fas fa-cog"></i></button>
                                </td>
                            </tr>
                            <tr class="cw-row">
                                <td class="cw-column">
                                    <input type="checkbox" name="" id="">
                                    <div class="cw-member-avatar">
                                        <img src="public/images/user/avatar1.jpg" alt="member's avatar" width="40" height="40">
                                    </div>
                                    <a href="#">KhaiTran</a>
                                </td>
                                <td class="cw-column">
                                    khaitran2504@gmail.com
                                </td>
                                <td class="cw-column">
                                    Creater
                                </td>
                                <td class="cw-column">
                                    <button class="btn btn-default"><i class="fas fa-cog"></i></button>
                                </td>
                            </tr>
                            <tr class="cw-row">
                                <td class="cw-column">
                                    <input type="checkbox" name="" id="">
                                    <div class="cw-member-avatar">
                                        <img src="public/images/user/avatar1.jpg" alt="member's avatar" width="40" height="40">
                                    </div>
                                    <a href="#">ToBich</a>
                                </td>
                                <td class="cw-column">
                                    bichto@gmail.com
                                </td>
                                <td class="cw-column">
                                    Creater
                                </td>
                                <td class="cw-column">
                                    <button class="btn btn-default"><i class="fas fa-cog"></i></button>
                                </td>
                            </tr>   
                        </tbody>
                    </table>
                </div>
            </div> 
        </div>
        <!-- /content -->
        <div class="clearfix"></div>
    </div>
    <!-- /body -->
    <!-- footer -->
    <div class="html-template" w3-include-html="layout/footer.html"></div>
    <!-- /footer -->
    <script src="public/libs/jquery/dist/jquery.min.js"></script>
    <script src="public/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="public/libs/require/require.js"></script>
    <script src="public/build/js/script.js"></script>
</body>
</html>