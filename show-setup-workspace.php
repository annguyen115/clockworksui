<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Show setup workspace</title>
    <link rel="shortcut icon" type="image/png" href="public/images/favicon.png"/>
    <link rel="stylesheet" href="public/libs/font-awesome-5/css/all.css">
    <link rel="stylesheet" href="public/libs/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="public/build/css/style.css">
</head>
<body>
    <!-- header -->
    <div class="html-template" w3-include-html="layout/header.html"></div>
    <!-- /header -->
    <!-- body -->
    <div class="cw-body">
        <!-- menu -->
        <div class="html-template" w3-include-html="layout/menu.html"></div>
        <!-- /menu -->
        <!-- content -->
        <div class="cw-body-right cw-content">
            <div class="cw-workspace-header">
                <div class="cw-workspace-intro">
                    <div class="cw-workspace-avatar">
                        <img src="public/images/workspace/avatar1.jpg" alt="workspace's avatar">
                    </div>
                    <div class="cw-workspace-name cw-normal-header">
                        CapstoneProject-ClockWorks
                    </div>
                </div>
                <ul class="cw-tabs">
                    <li rel="process">
                        <i class="fas fa-sitemap"></i>&nbsp;Processes&nbsp;<span class="cw-tab-quantity">0</span>
                    </li>
                    <li rel="members">
                        <i class="fas fa-user-friends"></i>&nbsp;Members&nbsp;<span class="cw-tab-quantity">1</span>
                    </li>
                    <li class="cw-active-tab" rel="settings">
                        <i class="fas fa-cog"></i>&nbsp;Settings
                    </li>
                </ul>
            </div>
            <div class="cw-container">
                <div class="cw-normal-header">Workspace profile</div>
                <hr class="cw-normal-hr" style="margin-top:10px">
                <div class="cw-workspace-profile">
                    <div class="cw-workspace-profile-infor">
                        <form action="#" method="POST">
                            <div class="form-group">
                                <label for="cw-workspace-name" class="cw-required">Rename workspace</label>
                                <input type="text" name="name" class="form-control" id="cw-workspace-name">
                            </div>
                            <div class="form-group">
                                <label for="cw-workspace-email" class="cw-required">Billing email</label>
                                <input type="text" name="email" class="form-control" id="cw-workspace-email">
                            </div>
                            <div class="form-group">
                                <label for="cw-workspace-description" class="cw-required">Description</label>
                                <textarea name="description" class="form-control" id="cw-workspace-description" cols="30" rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success">Update profile</button>
                            </div>
                        </form>
                    </div>
                    <div class="cw-workspace-profile-avatar">
                        <div class="cw-label">Profile picture</div>
                        <div class="cw-workspace-avatar cw-big-image">
                            <img src="public/images/workspace/avatar1.jpg" alt="workspace's avatar">
                        </div>
                        <button class="btn btn=default">Upload new picture</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="cw-error-area">
                    <div class="cw-header-erorr-large">
                        Danger Zone
                    </div>
                </div>
            </div>  
        </div>
        <!-- /content -->
        <div class="clearfix"></div>
    </div>
    <!-- body -->

    <!-- footer -->
    <div class="html-template" w3-include-html="layout/footer.html"></div>
    <!-- /footer -->
    <script src="public/libs/jquery/dist/jquery.min.js"></script>
    <script src="public/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="public/libs/require/require.js"></script>
    <script src="public/build/js/script.js"></script>
</body>
</html>