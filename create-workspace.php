<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create Workspace</title>
    <link rel="shortcut icon" type="image/png" href="public/images/favicon.png"/>
    <link rel="stylesheet" href="public/libs/font-awesome-5/css/all.css">
    <link rel="stylesheet" href="public/libs/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="public/build/css/style.css">
</head>
<body>
    <!-- header -->
    <div class="html-template" w3-include-html="layout/header.html"></div>
    <!-- /header -->
    <!-- body -->
    <div class="cw-body">
        <!-- menu -->
        <div class="html-template" w3-include-html="layout/menu.html"></div>
        <!-- /menu -->
        <!-- content -->
        <div class="cw-body-right cw-content">
            <div class="cw-container">
                <div class="cw-title">Sign up your workspace</div>
                <ol class="cw-steps">
                    <li class="current">
                        <i class="fas fa-briefcase"></i>
                        <strong class="cw-step">Step 1:</strong>Set up your workspace
                    </li>
                    <li>
                        <i class="fas fa-user-plus"></i>
                        <strong class="cw-step">Step 2:</strong>Set up your workspace
                    </li>
                </ol>
                <div class="cw-create-workspace">
                    <form action="#" method="POST">
                        <h2 class="cw-header-light">Create your workspace right away!!</h2>
                        <div class="form-group">
                            <label for="cw-workspace-name" class="cw-required">Workspace name</label>
                            <input type="text" name="name" class="form-control" id="cw-workspace-name">
                            <span class="cw-hint">This will be your workspace name on our website</span>
                        </div>
                        <div class="form-group">
                            <label for="cw-workspace-email" class="cw-required">Billing Email</label>
                            <input type="email" name="email" class="form-control" id="cw-workspace-email">
                            <span class="cw-hint">We'll send receipts to this mail box</span>
                        </div>
                        <div class="form-group">
                            <label for="cw-workspace-description">Description</label>
                            <textarea name="description" class="form-control" id="cw-workspace-description" cols="30" rows="10"></textarea>
                            <span class="cw-hint">Describe your workspace that you want to establish</span>
                        </div>
                        <div>
                            <button class="btn btn-success">Create workspace</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /content -->
        <div class="clearfix"></div>
    </div>
    <!-- /body -->
    <!-- footer -->
    <div class="html-template" w3-include-html="layout/footer.html"></div>
    <!-- /footer -->
    <script src="public/libs/jquery/dist/jquery.min.js"></script>
    <script src="public/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="public/libs/require/require.js"></script>
    <script src="public/build/js/script.js"></script>
</body>
</html>