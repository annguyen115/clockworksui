<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Show Workspace</title>
    <link rel="shortcut icon" type="image/png" href="public/images/favicon.png"/>
    <link rel="stylesheet" href="public/libs/font-awesome-5/css/all.css">
    <link rel="stylesheet" href="public/libs/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="public/build/css/style.css">
</head>
<body>
    <!-- header -->
    <div class="html-template" w3-include-html="layout/header.html"></div>
    <!-- /header -->
    <div class="cw-body">
        <!-- menu -->
        <div class="html-template" w3-include-html="layout/menu.html"></div>
        <!-- /menu -->
        <!-- content -->
        <div class="cw-body-right cw-content">
            <div class="cw-workspace-header">
                <div class="cw-workspace-intro">
                    <div class="cw-workspace-avatar">
                        <img src="public/images/workspace/avatar1.jpg" alt="workspace's avatar">
                    </div>
                    <div class="cw-workspace-name cw-normal-header">
                        CapstoneProject-ClockWorks
                    </div>
                </div>
                <ul class="cw-tabs">
                    <li class="cw-active-tab" rel="process">
                        <i class="fas fa-sitemap"></i>&nbsp;Processes&nbsp;<span class="cw-tab-quantity">0</span>
                    </li>
                    <li rel="members">
                        <i class="fas fa-user-friends"></i>&nbsp;Members&nbsp;<span class="cw-tab-quantity">1</span>
                    </li>
                    <li rel="settings">
                        <i class="fas fa-cog"></i>&nbsp;Settings
                    </li>
                </ul>
            </div>
            <div class="cw-container">
                <div class="cw-find-process">
                    <form action="#" method="get">
                        <input type="email" name="email" class="form-control" id="cw-proccess-find" placeholder="Find a proccess">
                        <button class="btn btn-default">Find</button>
                    </form>
                </div>
                <div class="cw-find-process-pinned">
                    <span>Customize pinned proccess</span>
                    <button class="btn btn-success"><i class="fas fa-sitemap"></i> New</button>
                </div>
                <hr class="cw-normal-hr">
                <div class="cw-workspace-internal">
                    <div class="row" style="margin:0">
                        <div class="col-md-8 cw-workspace-list-process">
                            <ul class="cw-has-proccess">
                                <li>
                                    <div class="cw-process">
                                        <a href="#" class="cw-process-name">Capstone Project</a>
                                        <p class="cw-process-description">
                                            This is a college gradution project of Van Lang student
                                        </p>
                                    </div>
                                </li>
                                <li>
                                    <div class="cw-process">
                                        <a href="#" class="cw-process-name">Software Management</a>
                                        <p class="cw-process-description">
                                            This is a college gradution project of Van Lang student
                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-4" style="padding-right: 0">
                            <div class="col-md-12 cw-workspace-members cw-box">
                                <div class="cw-box-header">
                                    <div class="cw-box-title">Members</div>
                                    <div class="cw-member-total">1 <i class="fas fa-users"></i></div>
                                    <div class="clearfix"></div>
                                    <hr class="cw-normal-hr" style="margin-top: 10px">
                                </div>
                                <div class="cw-box-body">
                                    <ul class="cw-members-list media-list">
                                        <li class="media">
                                            <a href="#" class="cw-member-infor">
                                                <div class="media-left"><img alt="user's avatar" class="media-object" src="public/images/user/avatar1.jpg" style="width: 64px; height: 64px;"></div>
                                                <div class="media-body">
                                                    PhuongDuy
                                                </div>
                                            </a>
                                        </li>
                                        <li class="media">
                                            <a href="#" class="cw-member-infor">
                                                <div class="media-left"><img alt="user's avatar" class="media-object" src="public/images/user/avatar1.jpg" style="width: 64px; height: 64px;"></div>
                                                <div class="media-body">
                                                    AnNguyen
                                                </div>
                                            </a>
                                        </li>
                                        <li class="media">
                                            <a href="#" class="cw-member-infor">
                                                <div class="media-left"><img alt="user's avatar" class="media-object" src="public/images/user/avatar1.jpg" style="width: 64px; height: 64px;"></div>
                                                <div class="media-body">
                                                    KhaiTran
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
        <!-- /content -->
        <div class="clearfix"></div>
    </div>
    <!-- footer -->
    <div class="html-template" w3-include-html="layout/footer.html"></div>
    <!-- /footer -->
    <script src="public/libs/jquery/dist/jquery.min.js"></script>
    <script src="public/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="public/libs/require/require.js"></script>
    <script src="public/build/js/script.js"></script>
</body>
</html>