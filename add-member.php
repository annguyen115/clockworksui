<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add Members</title>
    <link rel="shortcut icon" type="image/png" href="public/images/favicon.png"/>
    <link rel="stylesheet" href="public/libs/font-awesome-4/css/font-awesome.min.css">
    <link rel="stylesheet" href="public/libs/font-awesome-5/css/all.css">
    <link rel="stylesheet" href="public/libs/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="public/build/css/style.css">
    <link rel="stylesheet" href="public/build/css/responsive.css">
</head>
<body class="nav-sm">
    <div class="container body">
        <div class="main_container">
            <!-- menu -->
            <?php
                include_once('layout/menu.html')
            ?>
            <!-- /menu -->
            <?php
                include_once('layout/header.html')
            ?>
            <!-- content -->
            <div class="right_col" id="body" role="main">
                <div class="">
                    <div class="cw-title">Invite members to your workspace</div>
                    <ol class="cw-steps">
                        <li class="active">
                            <i class="fas fa-briefcase"></i>
                            <strong class="cw-step">Step 1:</strong>Set up your workspace
                        </li>
                        <li class="current">
                            <i class="fas fa-user-plus"></i>
                            <strong class="cw-step">Step 2:</strong>Set up your workspace
                        </li>
                    </ol>
                    <div class="cw-add-member">
                        <form action="#" method="POST">
                            <div class="form-group">
                                <label for="cw-members-name" class="cw-required">Search by username, fullname or email address</label>
                                <span class="cw-input-icon"> 
                                    <input type="text" name="name" class="form-control cw-input-has-icon" id="cw-members-name">
                                    <i class="fas fa-user cw-label-icon"></i>
                                </span>
                                <button class="btn btn-default disabled">Invite</button>
                            </div>
                            <div>
                                <button class="btn btn-success">Continue</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /content -->    
            <!-- footer -->
            <?php
                include_once('layout/footer.html')
            ?>
            <!-- /footer -->
            
        </div>
    </div>
    <script src="public/libs/jquery/dist/jquery.min.js"></script>
    <script src="public/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="public/libs/require/require.js"></script>
    <script src="public/build/js/script.js"></script>
</body>
</html>